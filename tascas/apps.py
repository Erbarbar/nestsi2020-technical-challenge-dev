from django.apps import AppConfig


class TascasConfig(AppConfig):
    name = 'tascas'
