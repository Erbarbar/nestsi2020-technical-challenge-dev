from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db.models import Q
from tascas.models import Tasca
from tascas.forms import TascaForm

# Create your views here.


def tascas(request):
    context = {
        "Tascas": Tasca.objects.all(),
        "Form": TascaForm(),
    }
    return render(request, "tascas.html", context)


def detailTasca(request, tasca_pk):
    tasca = get_object_or_404(Tasca, pk=tasca_pk)
    context = {
        "Tasca": tasca,
    }
    return render(request, "detail.html", context)


def addTasca(request):
    if request.method == 'POST':
        # Create a form instance and populate it with data from the request (binding):
        form = TascaForm(request.POST, request.FILES)
        # Check if the form is valid:
        tasca = Tasca()
        updateTasca(tasca, form)
        # redirect to a new URL:
        return HttpResponseRedirect(reverse('tascas'))

    context = {
        "Form": TascaForm(),
    }
    return render(request, "form.html", context)


def editTasca(request, tasca_pk):
    tasca = get_object_or_404(Tasca, pk=tasca_pk)
    if request.method == 'POST':
        # Create a form instance and populate it with data from the request (binding):
        form = TascaForm(request.POST, request.FILES)
        updateTasca(tasca, form)
        # redirect to a new URL:
        return HttpResponseRedirect(reverse('tascas'))

    context = {
        "Form": TascaForm(instance=tasca),
        "Tasca": tasca,
    }
    return render(request, "form.html", context)


def updateTasca(tasca, form):
    if form.is_valid():
        tasca.name = form.cleaned_data['name']
        tasca.address = form.cleaned_data['address']
        tasca.rating = form.cleaned_data['rating']
        tasca.image = form.cleaned_data['image']
        tasca.latitude = form.cleaned_data['latitude']
        tasca.longitude = form.cleaned_data['longitude']
        tasca.save()


def deleteTasca(request, tasca_pk):
    tasca = get_object_or_404(Tasca, pk=tasca_pk)
    tasca.delete()
    return HttpResponseRedirect(reverse('tascas'))


def search(request):
    query = request.GET['search']
    tascas = Tasca.objects.filter(
        Q(name__icontains=query) | Q(
            address__icontains=query) | Q(
            rating__icontains=query)
    )
    context = {
        "Tascas": tascas
    }
    return render(request, 'tascas.html', context)
