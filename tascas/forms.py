from django import forms
from tascas.models import Tasca

# Create the form class.


class TascaForm(forms.ModelForm):
    class Meta:
        model = Tasca
        fields = ['name', 'address', 'rating',
                  'image', 'latitude', 'longitude']
        help_texts = {
            'rating': ('[1-10]'),
        }
