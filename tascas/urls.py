from django.urls import path
from tascas.views import *

urlpatterns = [
    path('<int:tasca_pk>/delete', deleteTasca, name="deleteTasca"),
    path('<int:tasca_pk>/edit', editTasca, name="editTasca"),
    path('<int:tasca_pk>', detailTasca, name="detailTasca"),
    path('search', search, name="search"),
    path('add', addTasca, name="addTasca"),
    path('', tascas, name='tascas'),
]
